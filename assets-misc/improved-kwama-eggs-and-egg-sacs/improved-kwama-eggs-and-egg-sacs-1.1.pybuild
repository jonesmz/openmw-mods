# Copyright 2019-2021 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
from pybuild import InstallDir, Pybuild1

from common.fix_maps import FixMaps
from common.nexus import NexusMod


class Package(FixMaps, NexusMod, Pybuild1):
    NAME = "Improved Kwama Eggs and Egg Sacs"
    DESC = "A mesh and texture replacer for the kwama eggs and egg sacs"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/43555"
    KEYWORDS = "openmw"
    LICENSE = "attribution-nc"
    NEXUS_URL = "https://www.nexusmods.com/morrowind/mods/43555"
    TEXTURE_SIZES = "512"
    SRC_URI = "Kwama_Egg_sacs_and_Eggs_Replacer-43555-1-1.7z"
    IUSE = "graphic-herbalism animation"
    NORMAL_MAP_PATTERN = "_nm"
    INSTALL_DIRS = [
        InstallDir("00-Data Files", REQUIRED_USE="!graphic-herbalism"),
        # From Modding-openmw.com: If you're using graphic-herbalism,
        #                          don't install any meshes from this
        InstallDir(
            "00-Data Files", BLACKLIST=["meshes"], REQUIRED_USE="graphic-herbalism"
        ),
        # Don't use the bump-mapped meshes.
        InstallDir("01-bump-maps", BLACKLIST=["meshes"]),
        InstallDir("02-Pulsing Animation", REQUIRED_USE="animation"),
        InstallDir(
            "03-Pulsing Animation Bump-mapped",
            BLACKLIST=["meshes"],
            REQUIRED_USE="animation",
        ),
        # InstallDir("04-Graphic herbalism"),
        # InstallDir("05-Graphic herbalism bump-mapped"),
    ]
