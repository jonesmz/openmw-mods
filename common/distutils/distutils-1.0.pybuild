# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import sys
import os
from pybuild import Pybuild1


class Package(Pybuild1):
    NAME = "Distutils"
    DESC = "Pybuild class for installing distutils-based python packages"
    KEYWORDS = "openmw tes3mp"


class Distutils(Pybuild1):
    """
    Pybuild class for distutils packages

    Note that this assumes the python package is version-independent.
    It just installs a single version into lib/python.
    """

    def setup_py(self, *args: str):
        # Note that we don't want to compile bytecode, as interpreter changes could
        # cause extra bytecode to be created, which wouldn't be removed when the
        # package is uninstalled. This is also why PYTHONDONTWRITEBYTECODE is set
        self.execute(
            f"{sys.executable} setup.py install --install-lib lib/python --install-scripts bin "
            f'--root "{self.D}" --install-data . --no-compile ' + " ".join(args)
        )

    def src_install(self):
        super().src_install()
        # In case the user has SETUPTOOLS_SCM installed, and the package optionally uses it
        # dev-python/setuptools does this, and it doesn't seem possible to disable system site packages
        # It doesn't seem possible t
        os.environ["SETUPTOOLS_SCM_PRETEND_VERSION"] = self.PV
        self.setup_py()
        for pattern in [
            "AUTHORS*",
            "BUGS*",
            "CHANGELOG*",
            "CHANGES*",
            "CREDITS*",
            "FAQ*",
            "NEWS*",
            "README*",
            "THANKS*",
            "TODO*",
        ]:
            self.dodoc(pattern)
