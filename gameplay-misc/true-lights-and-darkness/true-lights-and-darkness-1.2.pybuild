# Copyright 2019-2021 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from pybuild import File, InstallDir, Pybuild1

from common.nexus import NexusMod


class Package(NexusMod, Pybuild1):
    NAME = "True Lights and Darkness"
    DESC = "Darkens all interior cells and increases radius of all light sources"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/39605"
    # Incorporates parts of Mudcrab Island (which can be freely used,
    # except for some unlicensed assets, but TLAD only uses scripts from it)
    # Also incorporates parts of Lights300, which is unlicensed
    # TLAD itself has an attribution license, but this only applies to Booze's own work
    LICENSE = "attribution all-rights-reserved"
    RDEPEND = "base/morrowind[bloodmoon,tribunal]"
    DEPEND = "base/morrowind"
    KEYWORDS = "openmw"
    SRC_URI = (
        "TLAD_Lights_Only_Necro_Edit_Logical_Flicker_ExpSnd-39605-1-2-1613115944.7z"
    )
    NEXUS_URL = HOMEPAGE
    IUSE = "+daylight"
    SETTINGS = {
        "Shaders": {"clamp lighting": "false"},
    }
    FALLBACK = {
        "LightAttenuation": {
            "UseConstant": 1,
            "ConstantValue": 0.382,
            "UseLinear": 1,
            "LinearMethod": 1,
            "LinearValue": 1,
            "LinearRadiusMult": 1.0,
            "UseQuadratic": 1,
            "QuadraticMethod": 2,
            "QuadraticValue": 2.619,
            "QuadraticRadiusMult": 1.0,
            "OutQuadInLin": 0,
        }
    }

    INSTALL_DIRS = [
        InstallDir(
            ".",
            PLUGINS=[File("TLAD Lights-LogicalFlicker-OriginalColors-ExpSnd.esp")],
            S="TLAD_Lights_Only_Necro_Edit_Logical_Flicker_ExpSnd-39605-1-2-1613115944",
        ),
    ]
