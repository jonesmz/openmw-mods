- Mod Atom: (category/mod-name of the mod as it appears in this repository)
- Location: (homepage where the mod can be found. Probably the same as the homepage for the old version)
- Version: (the new version number)

## Structural Changes

(If changes have been made to the archive structure, it would be helpful to list them here)

(Otherwise, note that the structure of the mod is the same as the old version)

## Other changes

(Might include changes to the license, homepage, description, or even name)

/label updated-mod
