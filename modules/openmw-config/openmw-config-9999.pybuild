# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os
import shutil

from pybuild import Pybuild1
from pybuild.info import PN

from common.distutils import Distutils
from common.git import Git


class Package(Git, Distutils, Pybuild1):
    NAME = "Portmod OpenMW Config Module"
    DESC = "Sorts openmw.cfg and settings.cfg to match mods installed by portmod."
    LICENSE = "GPL-3"
    KEYWORDS = ""
    HOMEPAGE = f"https://gitlab.com/portmod/{PN}"
    GIT_SRC_URI = f"https://gitlab.com/portmod/{PN}.git"
    PROPERTIES = "module"
    S = PN
    IUSE = "grass"
    RDEPEND = "dev-python/configobj"

    def src_prepare(self):
        if "grass" in self.USE:
            self.SETTINGS = {
                "Groundcover": {
                    "enabled": "true",
                    "density": 0.5,
                    "min chunk size": 0.5,
                }
            }

    def src_install(self):
        os.makedirs(os.path.join(self.D, "modules"))
        shutil.move(
            "openmw-config.pmodule",
            os.path.join(self.D, "modules", "openmw-config.pmodule"),
        )
        super().src_install()
